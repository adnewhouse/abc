/*
 * oled.h
 *
 *  Created on: Nov 30, 2020
 *      Author: adnew
 */

#ifndef INC_OLED_H_
#define INC_OLED_H_

#include "font.h"

void oled_init();
void oled_write_cmd(uint8_t data);
void oled_write_data(uint8_t data);
void oled_set_col(unsigned char xStart, unsigned char xEnd);
void oled_set_row(unsigned char yStart, unsigned char yEnd);
void oled_clear();
void oled_write();
void oled_draw_graph(uint8_t val);
void oled_draw_char(char c, const GFXfont* font, uint8_t x, uint8_t y);


#endif /* INC_OLED_H_ */
