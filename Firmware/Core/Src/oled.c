/*
 * oled.c
 *
 *  Created on: Nov 30, 2020
 *      Author: adnew
 */
#include <stdint.h>
#include <string.h>
#include "main.h"
#include "oled.h"

uint8_t buf[8192]; // Frame buffer

void oled_init() {
	// Setup OLED Display
	oled_write_cmd(0xAE); //display OFF
	oled_write_cmd(0xB3); //set CLK div. & OSC freq.
	oled_write_data(0x91);
	oled_write_cmd(0xCA); //set MUX ratio
	oled_write_data(0x3F);
	oled_write_cmd(0xA2); //set offset
	oled_write_data(0x00);
	oled_write_cmd(0xAB); //function selection
	oled_write_data(0x01);
	oled_write_cmd(0xA0); //set re-map
	oled_write_data(0x16);
	oled_write_data(0x11);
	oled_write_cmd(0xC7); //master contrast current
	oled_write_data(0x0F);
	oled_write_cmd(0xC1); //set contrast current
	oled_write_data(0x0F);
	oled_write_cmd(0xB1); //set phase length
	oled_write_data(0xE2);
	oled_write_cmd(0xBB); //set pre-charge voltage
	oled_write_data(0x1F);
	oled_write_cmd(0xB4); //set VSL
	oled_write_data(0xA0);
	oled_write_data(0xFD);
	oled_write_cmd(0xBE); //set VCOMH
	oled_write_data(0x07);
	oled_write_cmd(0xA6); //set display mode
	oled_write_cmd(0xAF); //display ON*/
}

void oled_write_cmd(uint8_t data) {
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, 0);
	HAL_GPIO_WritePin(DC_GPIO_Port, DC_Pin, 0);

	HAL_SPI_Transmit(&hspi1,  &data, 1, 100);

	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, 1);
}

void oled_write_data(uint8_t data) {
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, 0);
	HAL_GPIO_WritePin(DC_GPIO_Port, DC_Pin, 1);

	HAL_SPI_Transmit(&hspi1,  &data, 1, 100);

	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, 1);
}

void oled_set_col(unsigned char xStart, unsigned char xEnd) {
	oled_write_cmd(0x15); //set column (x-axis) start/end address
	oled_write_data(xStart); //column start; 28 is left-most column
	oled_write_data(xEnd); //column end; 91 is right-most column
}

void oled_set_row(unsigned char yStart, unsigned char yEnd) {
	oled_write_cmd(0x75); //set row (y-axis) start/end address
	oled_write_data(yStart); //row start; 0 is top row
	oled_write_data(yEnd); //row end; 63 is bottom row
}

void oled_clear() {
	memset(buf, 0, 8192);
}

void oled_update() {
	oled_set_col(28,91); //set column (x-axis) start/end address
	oled_set_row(0,63); //set row (y-axis) start/end address
	oled_write_cmd(0x5C); //single byte command (0x5C) to initiate pixel data write to GDDRAM;

	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, 0);
	HAL_GPIO_WritePin(DC_GPIO_Port, DC_Pin, 1);

	HAL_SPI_Transmit(&hspi1, buf, 8192, 100); // Push out the frame buffer all together

	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, 1);
}

void oled_draw_graph(uint8_t val) {
	for(int i = 0; i < val; i++) {
		buf[i + (60*128)] = 0xff;
		buf[i + (61*128)] = 0xff;
		buf[i + (62*128)] = 0xff;
		buf[i + (63*128)] = 0xff;
	}
}

void oled_draw_char(char c, const GFXfont* font, uint8_t x, uint8_t y) {
	c -= font->first;
	GFXglyph *glyph = font->glyph + c;
	uint8_t* bitmap = font->bitmap;
	uint16_t bo = glyph->bitmapOffset;
	uint8_t width = glyph->width;
	uint8_t height = glyph->height;
	int8_t xo = glyph->xOffset;
	int8_t yo = glyph->yOffset;

	// What follows is stolen from Adafruit GFX library
	// It is really ugly :(
	uint8_t xx, yy, bits = 0, bit = 0;

	for (yy = 0; yy < height; yy++) {
		for (xx = 0; xx < width; xx++) {
			if (!(bit++ & 7)) {
				bits = bitmap[bo++];
			}
			if (bits & 0x80) {
				buf[ x + xo + xx + (128 * (y + yo + yy)) ] = 0xFF;
			}
			bits <<= 1;
		}
	}
}














